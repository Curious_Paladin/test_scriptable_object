﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
    /**
    * Мы объявляем поле того же класса, что и наш ScriptableObject
    * Благодаря этому мы сможем использовать наш ранее созданный конфиг в этом объекте.
    **/
    [SerializeField] Enemy enemyConfig;

    // Компоненты, значения для которых мы возьмём из ScriptableObject
    private SpriteRenderer spriteRendererComponent;
    private TextMesh textMeshComponent;

    void Start() {
        GetObjectComponents();
        SetObjectValues();
    }

    // Получаем компоненты, значения которых мы будем изменять
    private void GetObjectComponents() {
        spriteRendererComponent = gameObject.GetComponent<SpriteRenderer>();
        textMeshComponent = gameObject.transform.GetChild(0).GetComponent<TextMesh>();
    }

    // Берём значения из ScriptableObject, и применяем их к нашим компонентам
    private void SetObjectValues() {
        spriteRendererComponent.sprite = enemyConfig.enemySprite;
        textMeshComponent.text = enemyConfig.enemyName;
    }
}
