﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* Данный атрибьют встривает создание нового объекта в меню assets/create
*
* fileName - имя файла, созданного из данного ScriptableObject. 
* Если оставить пустым, название будет взято по имени класса.
* menuName - меню или подменю, в котором будет распологаться создание нашего объекта.
* Если оставить пустым, то объект появится в основном меню.
* order - порядок расположения в меню или подменю.
**/
[CreateAssetMenu(fileName = "Enemy", menuName = "Configs/Enemy", order = 0)]
public class Enemy : ScriptableObject {
    // Создаём приватные переменные для характеристик нашего объекта

    // Спрайт 
    [SerializeField] Sprite sprite;
    // Имя
    [SerializeField] string name;

    /**
    * Для получения необходимых нам данных, лучше всего исполтзоввать систему геттеров/суттеров.
    * get - позволяет получить нужное нам свойство.
    * set - позволяет изменить нужное нам свойство. В данном примере задействовано не будет.
    **/
    public Sprite enemySprite {
        get {
            return sprite;
        }
    }

    public string enemyName {
        get {
            return name;
        }
    }
}
