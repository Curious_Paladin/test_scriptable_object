﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerBehaviour : MonoBehaviour {
    // Получаем конфиг с указанием точек спауна
    [SerializeField] Points pointsConfig;
    // Префабы врагов
    [SerializeField] List<GameObject> enemyPrefabs;
    void Start() {
        // Проходим циклом по точкам спауна врагов
        foreach (Vector3 position in pointsConfig.enemySpawnPoints) {
            // Добавляем врагов в игру
           Instantiate(
               // Выбираем случайный префаб из списка enemyPrefabs
               enemyPrefabs[Random.Range(0, enemyPrefabs.Count)], 
               // Подставляем точку спауна
               position, 
               // Ассоциируем созданный объект с игровым миром
               Quaternion.identity
            );
        }
    }
}
