﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Points", menuName = "Configs/Points", order = 1)]
public class Points : ScriptableObject {
    // Объект-контейнер с точками спауна врагов
    [SerializeField] GameObject pointsContainer;

    // "Распаковываем" точки спауна врагов из контейнра
    private List<Vector3> ExtractPositionPoints() {
        var vectorList = new List<Vector3>();

        foreach (Transform child in pointsContainer.transform) {
            vectorList.Add(child.transform.transform.position);
        }

        return vectorList;    
    }

    // Геттер, при помощи которого можно получить наше значение
    public List<Vector3> enemySpawnPoints {
        get {
            return ExtractPositionPoints();
        }
    }
}
